package com.n26.controller;

import com.n26.Application;
import com.n26.service.TransactionService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
@WebAppConfiguration
public class TransactionIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    TransactionService transactionService;

    private MockMvc mockMvc;

    private static final String TRANSACTIONS_ENDPOINT = "/transactions";
    private static final String STATISTICS_ENDPOINT = "/statistics";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
        this.transactionService.delete();
    }

    @Test
    public void testMalformedJSON() throws Exception {
        this.mockMvc.perform(post(TRANSACTIONS_ENDPOINT)
                            .content("NO_JSON")
                            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is(400));
    }

    @Test
    public void testNonParsableAmount() throws Exception {
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"amount\": \"not_a_number\"," +
                        "\"timestamp\": \"2018-10-24T19:58:37.470Z\"" +
                        "}")
        )
        .andExpect(status().is(422));
    }

    @Test
    public void testDateWrongFormat() throws Exception {
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"125\"," +
                                "\"timestamp\": \"2018-10-24 19:58:37.470\"" +
                                "}")
        )
                .andExpect(status().is(422));
    }

    @Test
    public void testCreateTransaction() throws Exception {
        Instant instant = Instant.now();
        String timeStamp = DateTimeFormatter.ISO_INSTANT.format(instant);
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"125\"," +
                                "\"timestamp\": \""+ timeStamp + "\"" +
                                "}")
        )
                .andExpect(status().is(201));
    }

    @Test
    public void testCreateFutureTransaction() throws Exception {
        Instant instant = Instant.now().plusSeconds(10);
        String timeStamp = DateTimeFormatter.ISO_INSTANT.format(instant);
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"125\"," +
                                "\"timestamp\": \""+ timeStamp + "\"" +
                                "}")
        )
                .andExpect(status().is(422));
    }

    @Test
    public void testDelete() throws Exception {
        // Create a transaction
        Instant instant = Instant.now();
        String timeStamp = DateTimeFormatter.ISO_INSTANT.format(instant);
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"125\"," +
                                "\"timestamp\": \""+ timeStamp + "\"" +
                                "}")
        )
                .andExpect(status().is(201));


        // Get statistics when we still have 1 transaction in the stats
        this.mockMvc.perform(get(STATISTICS_ENDPOINT))
            .andExpect(jsonPath("$.count").value(1));

        // execute delete
        this.mockMvc.perform(delete(TRANSACTIONS_ENDPOINT))
            .andExpect(status().is(204));

        // Check that we don't have any transactions in statistics now
        this.mockMvc.perform(get(STATISTICS_ENDPOINT))
                .andExpect(jsonPath("$.count").value(0));
    }

    @Test
    public void testStatistics() throws Exception {
        // Create a transaction
        Instant instant = Instant.now();
        String timeStamp = DateTimeFormatter.ISO_INSTANT.format(instant);
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"125.23432\"," +
                                "\"timestamp\": \""+ timeStamp + "\"" +
                                "}")
        )
                .andExpect(status().is(201));

        // create a 2nd transaction
        this.mockMvc.perform(
                post(TRANSACTIONS_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"amount\": \"21.2456\"," +
                                "\"timestamp\": \""+ timeStamp + "\"" +
                                "}")
        )
                .andExpect(status().is(201));

        this.mockMvc.perform(get(STATISTICS_ENDPOINT))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.count").value(2))
                .andExpect(jsonPath("$.sum").value("146.48"))
                .andExpect(jsonPath("$.min").value("21.25"))
                .andExpect(jsonPath("$.max").value("125.23"))
                .andExpect(jsonPath("$.avg").value("73.24"));

    }
}
