package com.n26.service;

import com.n26.Application;
import com.n26.datatransferobject.TransactionDTO;
import com.n26.domain.Summary;
import com.n26.exceptions.TransactionExpired;
import com.n26.exceptions.TransactionInFutureException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@ActiveProfiles(profiles = "unit-test")
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Before
    public void cleanup() {
        transactionService.delete();
    }

    @Test
    public void testSingleTransaction() throws TransactionExpired, TransactionInFutureException {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(10));
        transactionDTO.setTimestamp(ZonedDateTime.now());

        transactionService.add(transactionDTO);

        Summary summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        Assert.assertEquals(1, summary.getCount());
        Assert.assertEquals(new BigDecimal(10.00), summary.getMax());
        Assert.assertEquals(new BigDecimal(10.00), summary.getMin());
        Assert.assertEquals(new BigDecimal(10.00), summary.getSum());
        Assert.assertEquals(new BigDecimal("10.00"), summary.getAvg());
    }

    @Test(expected = TransactionInFutureException.class)
    public void testTransactionInFuture() throws TransactionExpired, TransactionInFutureException {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(10));
        transactionDTO.setTimestamp(ZonedDateTime.now().plus(1, ChronoUnit.SECONDS));

        transactionService.add(transactionDTO);
    }

    @Test(expected = TransactionExpired.class)
    public void testTransactionExpired() throws TransactionExpired, TransactionInFutureException {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(10));
        transactionDTO.setTimestamp(ZonedDateTime.now().minus(66, ChronoUnit.SECONDS));

        transactionService.add(transactionDTO);
    }

    @Test
    public void testTransactionExpiry() throws TransactionExpired, TransactionInFutureException, InterruptedException {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(10));
        // transaction that is 59 seconds in the past, close to expiry
        transactionDTO.setTimestamp(ZonedDateTime.now().minus(59, ChronoUnit.SECONDS));

        transactionService.add(transactionDTO);

        Summary summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        Assert.assertEquals(1, summary.getCount());

        // TODO: An improvement would be to look into adding adding a Clock into the system
        // which can be Mocked and manipulated for the sake of running the unit test
        // not done here because of time restrictions
        Thread.sleep(1000);

        summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        Assert.assertEquals(0, summary.getCount());
        Assert.assertEquals(new BigDecimal(0), summary.getMax());
        Assert.assertEquals(new BigDecimal(0), summary.getMin());
        Assert.assertEquals(new BigDecimal(0), summary.getSum());
        Assert.assertEquals(new BigDecimal(0), summary.getAvg());
    }

    @Test
    public void deleteTransactions() throws TransactionExpired, TransactionInFutureException {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(10));
        transactionDTO.setTimestamp(ZonedDateTime.now());

        transactionService.add(transactionDTO);

        Summary summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        Assert.assertEquals(1, summary.getCount());

        transactionService.delete();

        summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        Assert.assertEquals(0, summary.getCount());
    }

    @Test
    public void testTransactionExpiration2() throws TransactionExpired, TransactionInFutureException, InterruptedException {
        for (int i = 0; i < 10; i++) {
            int offset = i - 59;
            TransactionDTO transactionDTO = createTransaction(i*10 + 1, offset);
            transactionService.add(transactionDTO);
        }

        Summary summary = transactionService.getSummary();

        Assert.assertNotNull(summary);
        // should have 10 transactions
        Assert.assertEquals(10, summary.getCount());
        // Check min and max
        Assert.assertEquals(new BigDecimal(91), summary.getMax());
        Assert.assertEquals(new BigDecimal(1), summary.getMin());
        Assert.assertEquals(new BigDecimal(460), summary.getSum());

        // Sleep 1 second
        Thread.sleep(1000);

        // 1 transaction should have expired
        summary = transactionService.getSummary();
        Assert.assertEquals(9, summary.getCount());
        // validate that the min transaction was expired
        Assert.assertEquals(new BigDecimal(11), summary.getMin());
        Assert.assertEquals(new BigDecimal(459), summary.getSum());

        // Sleep another 2 seconds
        Thread.sleep(2000);

        // 2 transaction extra should have expired
        summary = transactionService.getSummary();
        Assert.assertEquals(7, summary.getCount());
        // validate that the min transactions were expired
        Assert.assertEquals(new BigDecimal(31), summary.getMin());
        Assert.assertEquals(new BigDecimal(427), summary.getSum());
    }

    private TransactionDTO createTransaction(int value, int offset) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(new BigDecimal(value));
        transactionDTO.setTimestamp(ZonedDateTime.now().plus(offset, ChronoUnit.SECONDS));
        return transactionDTO;
    }

}
