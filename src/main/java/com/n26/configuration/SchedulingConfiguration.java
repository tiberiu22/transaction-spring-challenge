package com.n26.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Configuration class that is used to enable scheduling in spring.
 * Can be disabled by setting `scheduling.enabled` to `false`.
 * NOTE: This is put in a separate config class so it can be easily disabled/enabled.
 * It is enabled by default.
 */
@Configuration
@EnableScheduling
@Profile("!unit-test")
@ConditionalOnProperty(prefix = "scheduling", name = "enabled", havingValue = "true", matchIfMissing = true)
public class SchedulingConfiguration {
}
