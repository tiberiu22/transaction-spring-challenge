package com.n26.datatransferobject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.n26.datatransferobject.serializer.BigDecimalTwoDigitsSerializer;
import lombok.*;

import java.math.BigDecimal;

@ToString
@Data
@NoArgsConstructor
public class StatisticsDTO {

    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal sum;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal avg;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal max;
    @JsonSerialize(using = BigDecimalTwoDigitsSerializer.class)
    private BigDecimal min;
    private long count = 0;

}
