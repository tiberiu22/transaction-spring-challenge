package com.n26.datatransferobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class TransactionDTO {

    private BigDecimal amount;
    @JsonProperty("timestamp")
    private ZonedDateTime timestamp;

    public ZonedDateTime getTimestampInUTC() {
        return timestamp.withZoneSameInstant(ZoneId.of("UTC"));
    }
}
