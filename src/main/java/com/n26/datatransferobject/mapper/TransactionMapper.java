package com.n26.datatransferobject.mapper;

import com.n26.datatransferobject.TransactionDTO;
import com.n26.domain.TransactionDO;

public class TransactionMapper {

    public static TransactionDO createTransactionDO(TransactionDTO transactionDTO) {
        if (transactionDTO == null) {
            throw new NullPointerException("transactionDTO is null");
        }

        TransactionDO transactionDO = new TransactionDO();
        transactionDO.setAmount(transactionDTO.getAmount());
        transactionDO.setTimestamp(transactionDTO.getTimestampInUTC());

        return transactionDO;
    }

    public static TransactionDTO createTransactionDTO(TransactionDO transactionDO) {
        if (transactionDO == null) {
            throw new NullPointerException("transactionDO is null");
        }

        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setAmount(transactionDO.getAmount());
        transactionDTO.setTimestamp(transactionDO.getTimestamp());

        return transactionDTO;
    }
}
