package com.n26.datatransferobject.mapper;

import com.n26.datatransferobject.StatisticsDTO;
import com.n26.domain.Summary;

import java.math.BigDecimal;

public class StatisticsMapper {

    public static StatisticsDTO createStatisticsDTO(Summary summary) {
        if (summary == null) {
            throw new NullPointerException("summary is missing");
        }

        StatisticsDTO statisticsDTO = new StatisticsDTO();
        statisticsDTO.setMax(summary.getMax() != null ? summary.getMax() : new BigDecimal(0));
        statisticsDTO.setMin(summary.getMin() != null ? summary.getMin() : new BigDecimal(0));
        statisticsDTO.setCount(summary.getCount());
        statisticsDTO.setSum(summary.getSum());
        statisticsDTO.setAvg(summary.getAvg());

        return statisticsDTO;
    }

}
