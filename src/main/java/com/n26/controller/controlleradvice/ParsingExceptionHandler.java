package com.n26.controller.controlleradvice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.format.DateTimeParseException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ParsingExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Class rootCause = ex.getRootCause().getClass();
        if (rootCause.equals(InvalidFormatException.class) || rootCause.equals(DateTimeParseException.class)) {
            return super.handleHttpMessageNotReadable(ex, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
        }
        else {
            return super.handleHttpMessageNotReadable(ex, headers,status, request);
        }
    }

}
