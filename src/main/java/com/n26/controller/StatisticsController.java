package com.n26.controller;

import com.n26.datatransferobject.StatisticsDTO;
import com.n26.datatransferobject.mapper.StatisticsMapper;
import com.n26.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    TransactionService transactionService;

    @GetMapping
    public StatisticsDTO get() {
        return StatisticsMapper.createStatisticsDTO(transactionService.getSummary());
    }
}
