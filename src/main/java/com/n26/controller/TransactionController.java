package com.n26.controller;

import com.n26.datatransferobject.TransactionDTO;
import com.n26.exceptions.TransactionExpired;
import com.n26.exceptions.TransactionInFutureException;
import com.n26.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transactions")
public class TransactionController {


    @Autowired
    private TransactionService transactionService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void post(@RequestBody TransactionDTO transaction) throws TransactionExpired, TransactionInFutureException {
        transactionService.add(transaction);
    }


    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete() {
        transactionService.delete();
    }
}
