package com.n26.service;

import com.n26.datatransferobject.TransactionDTO;
import com.n26.domain.Summary;
import com.n26.exceptions.TransactionExpired;
import com.n26.exceptions.TransactionInFutureException;

public interface TransactionService {

    public void add(TransactionDTO transaction) throws TransactionInFutureException, TransactionExpired;
    public void delete();
    public Summary getSummary();
}
