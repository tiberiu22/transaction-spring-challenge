package com.n26.service;

import com.n26.datatransferobject.TransactionDTO;
import com.n26.datatransferobject.mapper.TransactionMapper;
import com.n26.domain.Summary;
import com.n26.domain.TransactionDO;
import com.n26.domain.TransactionStorage;
import com.n26.exceptions.TransactionExpired;
import com.n26.exceptions.TransactionInFutureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionStorage transactionStorage;

    /**
     * Checks if a transaction is valid.
     *
     * @param transaction to check
     * @return true if the transaction is valid
     * @throws TransactionExpired if the transaction is older than 60 seconds
     * @throws TransactionInFutureException if the transaction is in the future
     */
    private boolean isValid(TransactionDTO transaction) throws TransactionExpired, TransactionInFutureException {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        if (now.minus(TransactionStorage.VALID_PERIOD_IN_SECONDS, ChronoUnit.SECONDS).isAfter(transaction.getTimestampInUTC())) {
            throw new TransactionExpired("Transaction expired already");
        }

        if (now.isBefore(transaction.getTimestampInUTC())) {
            throw new TransactionInFutureException("Transaction is in future");
        }

        return true;
    }

    /**
     * Adds a transaction to memory.
     * NOTE: Must complete in O(1)
     *
     * @param transaction
     * @throws TransactionInFutureException
     * @throws TransactionExpired
     */
    public void add(TransactionDTO transaction) throws TransactionInFutureException, TransactionExpired {
        if (! isValid(transaction)) {
            return;
        }

        TransactionDO transactionDO = TransactionMapper.createTransactionDO(transaction);
        transactionStorage.registerTransaction(transactionDO);

        transactionStorage.writeCleanup();
    }

    /**
     * Returns summary of the last 60 seconds of transactions.
     *
     * @return calculated summary
     */
    public Summary getSummary() {
        // If we need to do the cleanup before reading it will increase the complexity of the application,
        // but in a high throughput application with lots of writes this should happen rarely
        transactionStorage.readCleanup();
        return transactionStorage.getSummary();
    }

    /**
     * Deletes all transactions currently in memory.
     *
     */
    public void delete() {
        transactionStorage.deleteTransactions();
    }

}
