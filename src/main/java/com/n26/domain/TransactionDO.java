package com.n26.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class TransactionDO {

    private BigDecimal amount;
    private ZonedDateTime timestamp;

}
