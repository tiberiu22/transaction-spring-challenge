package com.n26.domain;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TransactionStorage {

    // the period in seconds that a transaction is valid for
    public static final int VALID_PERIOD_IN_SECONDS = 60;

    private Summary summary = new Summary();

    // Store transactions as a map of the transaction timestamp to a list of transactions in case there is more
    // than one transaction at the same time
    private Map<ZonedDateTime, ArrayList<TransactionDO>> transactions = new HashMap<>();

    private long lastCleanupTimestamp = System.currentTimeMillis();

    /**
     * Returns the calculated summary of transactions in the last 60 seconds.
     *
     * @return summary
     */
    public synchronized Summary getSummary() {
        return summary;
    }

    /**
     * Deletes all transactions.
     *
     */
    public synchronized void deleteTransactions() {
        summary = new Summary();
        transactions.clear();
    }

    /**
     * Registers a transaction in the cache and adds it to the summary.
     *
     * @param transaction to add
     */
    public synchronized void registerTransaction(TransactionDO transaction) {
        addToTimeMap(transaction);
        addToStatistics(transaction);
    }

    /**
     * Adds a transaction to the map used to keep track of the timestamp.
     *
     * @param transaction to add
     */
    private void addToTimeMap(TransactionDO transaction) {
        ArrayList<TransactionDO> transactionList = transactions.get(transaction.getTimestamp());

        if (transactionList != null) {
            transactionList.add(transaction);
        }
        else {
            transactions.put(transaction.getTimestamp(), new ArrayList<>(Arrays.asList(transaction)));
        }
    }

    /**
     * Adds a transaction to the summary.
     *
     * @param transaction to add
     */
    private void addToStatistics(TransactionDO transaction) {
        summary.setCount(summary.getCount()+1);

        if (summary.getMin() == null || transaction.getAmount().min(summary.getMin()) == transaction.getAmount()) {
            summary.setMin(transaction.getAmount());
        }

        if (summary.getMax() == null || transaction.getAmount().max(summary.getMax()) == transaction.getAmount()) {
            summary.setMax(transaction.getAmount());
        }

        summary.setSum(summary.getSum().add(transaction.getAmount()));

    }


    /**
     * Cleans up the transaction cache after each write in a different thread.
     */
    @Async
    public synchronized void writeCleanup() {
        cleanup();
    }

    /**
     * Runs a cleanup before reading if the cleanup hasn't been ran in the last 50 milliseconds.
     *
     */
    public synchronized void readCleanup() {
        // Only do the cleanup if there has been more than 50 milliseconds since the last cleanup
        long timeSinceLastCleanup = System.currentTimeMillis() - lastCleanupTimestamp;
        if (timeSinceLastCleanup < 50) {
            return;
        }

        cleanup();
    }

    /**
     * Cleans up the transaction cache every second.
     * Keeping this scheduled task so as to clean the cache often, so the cleanups should be kept fairly small in general.
     * Might not be necessary in a high throughput application.
     *
     */
    @Scheduled(fixedRate = 1000)
    protected synchronized void cleanup() {
        ZonedDateTime timeStampThreshold = ZonedDateTime.now(ZoneId.of("UTC")).minus(VALID_PERIOD_IN_SECONDS, ChronoUnit.SECONDS);
        Iterator<ZonedDateTime> timeStampIterator = transactions.keySet().iterator();
        BigDecimal sumRemoved = new BigDecimal(0);
        long removedCounter = 0L;
        while (timeStampIterator.hasNext()) {
            ZonedDateTime currentTimeStamp = timeStampIterator.next();
            // Stop iterating through the keys once the timestamp is after our threshold
            if (currentTimeStamp.isAfter(timeStampThreshold)) {
                continue;
            }

            // recalculate statistics
            for (TransactionDO transaction : transactions.get(currentTimeStamp)) {
                sumRemoved = sumRemoved.add(transaction.getAmount());
                removedCounter++;
            }

            timeStampIterator.remove();
        }

        // Update summary after we finish removing expired transactions, as to not complicate min/max calculation if there
        // are multiple transactions with the same amount
        summary.setSum(summary.getSum().subtract(sumRemoved));
        summary.setCount(summary.getCount() - removedCounter);
        calculateMinAndMax(transactions.values().stream().flatMap(list -> list.stream()).collect(Collectors.toList()));
        lastCleanupTimestamp = System.currentTimeMillis();
    }

    /**
     * Calculates the min and max transaction amount for a given
     * transaction list and updates it in the summary.
     *
     * @param transactionDOList list of current transactions
     */
    private void calculateMinAndMax(List<TransactionDO> transactionDOList) {
        List<BigDecimal> transactionAmounts = transactionDOList.stream()
                .map(TransactionDO::getAmount)
                .collect(Collectors.toList());

        if (transactionAmounts.isEmpty()) {
            summary.setMin(new BigDecimal(0));
            summary.setMax(new BigDecimal(0));
        }
        else {
            BigDecimal max = transactionAmounts.get(0);
            BigDecimal min = transactionAmounts.get(0);
            for (BigDecimal value : transactionAmounts) {
                if (value.max(max) == value) {
                    max = value;
                }
                if (value.min(min) == value) {
                    min = value;
                }
            }
            summary.setMax(max);
            summary.setMin(min);
        }
    }

}
