package com.n26.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class Summary {

    private BigDecimal sum;
    private BigDecimal max;
    private BigDecimal min;
    private long count;

    public Summary() {
        this.sum = new BigDecimal(0);
        this.count = 0;
    }

    public BigDecimal getAvg() {
        if (count == 0) {
            return new BigDecimal(0);
        }

        return sum.divide(new BigDecimal(count),2, RoundingMode.HALF_UP);
    }

}
